# Aetherblade

**Optimal Utility**: Potion of Slaying Scarlet's Armies  
**Day**/Night

## Mine Field
Swim through the underwater cave while avoiding the mines. Mines will kill you (or the person behind you) instantly, so proceed with caution. The mines will not trigger if you are in stealth, so stacking stealth and swiftness is an easy way to get through this section.

## Electric Floor
Once you make it through the mine field, you will need to complete two trash encounters. These are fairly straightforward, but beware of the Aetherblade strikers, who can stunlock you with their lightning attacks. Once that is done, you will have to cross the electric floor; black tiles are safe and purple tiles will down you instantly. Carefully cross to the hut on the other side, kill the inquest guard, and activate the terminals to disable the electric floor and move on to the next section.

**Note 1** The hologram next to this hut is the first of two that you will need to dance with to get an achievement.

**Note 2** Thieves, mesmers, and elementalists are able to teleport through the floor near the hut and turn off the electric floor without having to cross it.

## Laser Trap Room
After fighting another Inquest guard, you'll enter a room that forces you to navigate through a maze of spinning lasers. These lasers will stun and damage you when they hit you, so take care. Activate all of the consoles in the room within a short time frame to proceed, though the consoles will expire shortly after being hit and will need to be activated again if the trap is not disabled. It is ideal to have one party member at each console.

**Note 1** The hologram in the middle of this room is the second that you will have to dance with for the achievement.

**Note 2** Thieves, mesmers, and elementalists can teleport through the floor from the hut

## Barrage
A fairly straight forward encounter. Avoid the AoEs while dealing with the Aetherblades that spawn. After some time, the door at the far end will open, and you'll have another trash fight before you can move on to the final encounter.

## Frizz
This fight comes in four parts, and they are as follows:

### Part One
Fight off the trash and bring Frizz down to 75%. Once this is done, Frizz will teleport into the center of the room and activate the first electric wall, beginning the second phase of the fight. Grenadiers deal a lot of damage, and should be prioritized.

### Part Two
Continue fighting the golems and Inquest that spawn, while avoiding the electric wall, which will damage and stun you if it hits. Use the barrels and crates around the room to jump over the wall. Once enough golems are killed, the wall will be changed to a larger, slower moving wall, beginning the third phase of the fight. Take care around the golems, as they can pull you through the wall.

### Part Three
More of the same, except that this wall can't be jumped over. Fortunately, it moves slow enough that you can circle the room to avoid it. Continue killing golems until the final phase starts.

### Part Four
For this phase, you will have to deal with both walls. You will need to jump over the smaller. faster wall, while moving around the room to avoid the larger, slower wall. Once the last of the golems are dead, you will get a cutscene and your chest.

**Note** Invulnerability skills, such as the Mesmer's distortion, or the Guardian's Renewed Focus elite skill, can be used to get through the walls if you're not able to avoid them.