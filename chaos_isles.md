# Chaos Isles

**Optimal Utility**: None  
**Day**/Night

## Cliffside/Uncategorized
The fractal starts off with an encounter similar to the beginning of Cliffside, where you are fighting a series of trash mobs. This fight is fairly straight forward, except that as the fight progresses, they will change into the bosses from the first section of Uncategorized (rabbit, bandit, shaman, and ettin). Kill all of the enemies to proceed to the next section.

Once you've made it past the first encounter, you'll be presented with a jumping segment with harpy golems, who behave similarly to the harpies from uncategorized. These can either be killed, or you can run past them. Watch out for their AoE lightning attacks, as they will knock you off of the platform that you are on.

## Urban Battleground/Thaumanova Reactor
You will start the second part of the fractal by fighting a series of cat golems. BLIGHT relies on condition damage, DOC heals, PLINK uses ranged attacks, and CHOP uses melee attacks and stealth.

The main thing to watch out for with the anomaly is his Flux Bomb attack, which is indicated with a red target appearing over the player's head. This attack will place a damaging field around the player. If you are targeted with a flux bomb, move away from the group until the attack is finished.

At 75% the anomaly will become invulnerable, and three veteran cat golems will spawn (the last golem killed in the last phase will not spawn). At 50%, two elite golems will spawn, and at 25% a single champion golem will spawn.

**Note** There is an achievement for killing all four golems in their champion form. When working on this, ensure that the golem you need is never the last to be killed.

## Snowblind/Solid Ocean
To complete this section, four bonfires will need to be lit using the special action key. Coordination is key, as each player has only one light, and if there aren't enough lights for all of the bonfires, the event will fail. Once all of the fires are lit, the ice wall will come down, allowing the group to access the final boss.

Take care around the tentacles, as they will knock you down. Stability and swiftness can be helpful to get past the tentacles. Additionally, if you spend too much time away from a bonfire, you'll begin to accrue stacks of hypothermia, which does damage over time.

**Note** Because of their mobility, elementalists, mesmers, and thieves are good candidates to take the fourth fire.

## Brazen Gladiator (Aetherblade/Molten Facility)
The Brazen Gladiator will spawn with a shield that is stripped when hit by the electrified floor. If this shield appears again, lure him onto another electrified tile to remove it. These tiles will damage you, so try to stay out of them when possible. Other than that, he has a couple of things to watch out for. First, he has an auto attack chain consisting of three hits. The third hit, which has a longer wind-up than the first two, will daze you, so try to avoid it if possible. He also has a whirlwind attack that will down most players if it connects. This attack is telegraphed with a large orange circle, and it can be broken with crowd control effects.

On higher tier fractals, the thumpers from Molten Facility will strike the arena, sending out shockwaves that need to be jumped over. Harpy golems may also spawn, knocking players back with their lightning attack and dealing extra damage. These should be dealt with quickly.

## Journal Pages
Scattered throughout the fractal, there are a series of lost journal pages. There is an achievement for collecting all of them.

* To the left of the alter after the first trash fight.
* Instead of going down the path to the harpy golems, go up and complete a short jumping puzzle to reach a room containing the second page. Pull the nearby chain to reveal the platforms with the harpy golems.
* Once you've passed the harpy golems, turn right instead of going straight to the cat golems. There will be a platform with a tent that you will need to jump to to reach the third page.
* The fourth page is on top of the wall behind the anomaly boss.
* Continue past the fifth page, and up another jumping segment to reach a platform containing the fifth page.
* The sixth page is at the far north end of the cold area. It's best to wait until the wall is down before you start looking for this.
* The last page is on the platform just before you enter the arena for the Brazen Gladiator fight.