# Working with Supports

Learning to work with your supports in fractals/raids will not only make their jobs easier, but will also benefit yourself by making you more readily available to recieve buffs and healing. This will in turn give you more damage and lessen the chances of going down or wiping. 
**Note:** This is nto a guide for Druids and Chronomancers. It is a basic overview of positioning and using descriptions of abilities in order to benefit more from supporting characters. 
___
## *Druid* 
If you can learn to position yourself properly with your group's Druid, you can benefit from near constant heals, might generation, and health regeneration. 

### Positioning
Positioning with your Druid is possibly the most important thing to keep in mind when running with a Druid in your group. While the Druid is considered the "best" healing specialization, it is also heavily reliant on AoE heals and being in close proximity to apply a majority of those heals. The best way for a group to benefit from a Druid's healing and boons is to remain close and "stacking" in most boss encounters. 

### Healing Abilities
A Druid's main healing skills will be visible, but often not caught in the chaos of a fight.
 - Celestial Avatar (CA) is probably the most visible of the healing abilities, the Druid with turn into a glowing blue and starry figure and have increased healing skills and output. The main heal skill used in this form is an AoE, the edges clearly visible by a water particle effect, around the Druid with constant healing for a few seconds. 
![CA](https://i.imgur.com/Lp0py72.jpg?1 "Celestial Avatar")
 - The second visible heal you will see from a Druid is a Seed of Lesser healing. When a druid uses a glyph skill, they leave behind a seed with it's own range indicator. The lavender coloured seed will grow for a moment before bursting with healing in it's given range. Note that it is a stationary skill and takes a moment to burst. 
![SoLH](https://i.imgur.com/OAR0SnL.jpg?1 "Seed of Lesser Healing")
 - The last easily visible skill would be Astral Wisp. The Druid will send out a wisp of light to surround an enemy, healing players close enough to that enemy as it passes through them. 
Druid's healing skills aren't limited to these three but with their visual indicators, they should be easier to recognize and make sure you are in range to recieve healing. 
![Astral Wisp](https://i.imgur.com/GlKaIkh.jpg?1 "Astral Wisp")
___
## Healing and Boons Range
##### 300 units
![300](https://i.imgur.com/dUH35tx.jpg?1 "300 Units")

##### 600 units
![600](https://i.imgur.com/IMeSI1l.jpg?1 "600 Units")
___
## *Chronomancer*
As with Druid, there are certain skills that the Chronomancer uses that depend on positioning and range in order to share their boons and other support utilities. 

### Positioning
Chronomancers are considered some of the best tanks and boon sharing specialization. I'n order to recieve those boons you need to be in range, and as with Druid, it is heavily reliant on a few skills being very short range. Chronomancers also have wells which they can place down and it is important to stand in these wells in order to recieve their boons. Same as Druid, the best way to benefit from a Chronomancer's supporting skills is to stay in cloes proximity and stack on certain boss encounters. 
**Note:** Unlike Druid, Chronomancers don't have very clear visual indicators of their support abilities other than their wells. 

### Buffing Abilities
- Inspiring Distortion is a very close range ability that Chronos can use that will give their allies aegis when they give themselves distortion.
![Dist.](https://i.imgur.com/8qUsGhM.jpg?1 "Inspiring Distortion")
- Signet of Inspiration allows chronomancers to share one stack of each boon they have with their allies.
![SoI](https://i.imgur.com/Q71UHsz.jpg?2 "Signet of Inspiration")
- Tides of Time is a line AoE that allows Chronos to give alacrity to allies it passes through with their shield. 
![ToT](https://i.imgur.com/OYwUfNe.jpg?1 "Tides of Time")
- Chaos Storm is a ground Aoe that applies random boons to allies and random conditions to enemies. 
![CS](https://i.imgur.com/REOFHkm.jpg?2 "Chaos Storm")
- Time Warp is a large ground AoE that grants quickness to allies and slows enemies. 
![Let's do the Time Warp again](https://i.imgur.com/lHfMNdY.jpg?1 "Time Warp")

### Wells
Wells are large ground AoE's that give allies boons and apply conditions to enemies. It is always recommended that if you can, stand in your group's Chronomancer's wells.

- Well of Action damages and slows enemies, then when it expires grants Quickness to allies. 
![WoA](https://i.imgur.com/zj49XW0.jpg?1 "Well of Action")
- Well of Recall damages and chills enemies, when it expires it grants allies Alacrity. 
 ![WoR](https://i.imgur.com/qzGpCUy.jpg?1 "Well of Recall")
- Well of Precognition gives allies the ability to block, and when it expires grants allies Endurance.
![WoP](https://i.imgur.com/9EFN1mB.jpg?1 "Well of Precognition")